<?php require 'views/header.php'; ?>
<main>

        <h1>Inicio</h1>
        <h3>Error de clase</h3>
        <ul>
            <li>En sesión estaba el objeto $user</li>
            <li>Al llegar a home no se había incluido la clase User</li>
            <li>Hay que añadir un require_once en la clase Bootstrap</li>
        </ul>
        <h3>Tarea para el próximo día</h3>
        <ul>
            <li>Ve a la página /books</li>
            <li>Al pulsar en "recordar" debe guardarse ese objeto libro en sesión</li>
            <li>En todas las vistas debe aparece en el pie el último libro pulsado: titulo y autor</li>

        </ul>
</main>
<?php require 'views/footer.php'; ?>
