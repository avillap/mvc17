<?php require 'views/header.php'; ?>
<main>

    <h1>Edición de autor</h1>

    <form method="post" action="../update/<?php echo $author->id; ?>">
        <label>Nombre</label>
        <input type="text" name="name" value="<?php echo $author->name ?>">
        <br>

        <label>Apellidos</label>
        <input type="text" name="surname" value="<?php echo $author->surname ?>">
        <br>

        <label>F. Nacimiento</label>
        <input type="text" name="birthdate" value="<?php echo $author->birthdate ?>">
        <br>

        <input type="submit" value="Guardar cambios">
        <br>

    </form>
</main>
<?php require 'views/footer.php'; ?>

