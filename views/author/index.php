<?php require 'views/header.php'; ?>
<main>
    <div>

        <h1>Lista de autores</h1>

        <table>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Apellidos</th>
                <th>F. Nacimiento</th>
                <th>Acciones</th>
            </tr>
            <?php foreach ($authors as $author): ?>
                <tr>
                    <td><?php echo $author->id ?></td>
                    <td><?php echo $author->name ?></td>
                    <td><?php echo $author->surname ?></td>
                    <td><?php echo $author->birthdate ?></td>
                    <td>
                        <a href="<?php echo "delete/$author->id" ?>">borrar</a>
                        -
                        <a href="<?php
                        echo "edit/$author->id"
                        ?>">editar</a>
                    </td>
                </tr>
            <?php endforeach ?>
        </table>

        <p>
        <a href="create">Nuevo</a>
        </p>
    </div>

</main>
<?php require 'views/footer.php'; ?>
