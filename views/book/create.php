<!DOCTYPE html>
<html>
<head>
    <title>Alta de libro</title>
</head>
<body>
    <header>Cabecera<hr></header>

    <content>
        <h1>Alta de libro</h1>

        <form method="post" action="/book/store">
            <label>Título</label>
            <input type="text" name="title"><br>
            <label>Autor</label>
            <input type="text" name="author"><br>
            <label>Páginas</label>
            <input type="text" name="pages"><br>

            <label>Categoría</label>
            <select name="cathegory_id">
                <?php foreach ($cathegories as $cathegory): ?>
                    <option value="<?php echo $cathegory->id; ?>"> <?php echo $cathegory->name; ?></option>
                <?php endforeach ?>
            </select>
            <br>

            <input type="submit" value="Nuevo">
        </form>
    </content>

    <footer><hr>Pie de página</footer>
</body>
</html>
