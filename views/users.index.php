<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8"/>
    <title>Lista de usuarios</title>
</head>
<body>
    <header>Cabecera <hr></header>
    <content>
        <h1>Lista de usuarios</h1>
                <p><?php echo $data['nombre'] ?></p>
        <table>
            <tr>
                <th>Nombre</th>
                <th>Apellidos</th>
                <th>Edad</th>
                <th>Mail</th>
                <th>Acciones</th>
            </tr>

            <?php foreach ($data['users'] as $user): ?>
            <tr>
                <td> <?php echo $user->name; ?></td>
                <td> <?php echo $user->surname; ?></td>
                <td> <?php echo $user->age; ?></td>
                <td> <?php echo $user->email; ?></td>
                <td>
                    <a href="/mvc17/v4/user/edit/<?php echo $user->id?>"> Editar </a> -
                    <a href="/mvc17/v4/user/show/<?php echo $user->id?>"> Ver </a>
                </td>
            </tr>
            <?php endforeach ?>
        </table>
    </content>
    <footer> <hr> Pie</footer>
</body>
</html>
