<?php

/**
*
*/
require_once('app/Model.php');
require_once('Book.php');

class Cathegory extends Model
{
    public $id;
    public $name;

    function __construct()
    {
        # code...
    }

    public function find($id)
    {
        $db = Cathegory::connect();
        // echo "SELECT ...";
        $sql = "SELECT * FROM cathegories WHERE id = :id";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('id', $id);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Cathegory');
        return $stmt->fetch();
    }


    public function books()
    {
        //Si no existe el atributo cathegory buscar en BBDD
        if (!isset($this->books)) {
        //TODO: implementar método fromCathegory
            $this->books = Book::fromCathegory($this->cathegory_id);
        }
            //pegar el atributo al modelo

        //devolver el atributo del modelo book ($this-> ...)
        return $this->books;
    }
    public static function all()
    {
        $db = Cathegory::connect();

        $stmt = $db->prepare("SELECT * FROM cathegories");
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Cathegory');

        $results = $stmt->fetchAll();
        return $results;
    }

    public function __get($name)
    {
        return $this->$name();
    }
}
