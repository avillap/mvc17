<?php

/**
*
*/
class Model
{

    function __construct()
    {
        # code...
    }

    public static function connect()
    {
        $dsn = 'mysql:dbname=mvc17;host=127.0.0.1';
        $usuario = 'root';
        $contraseña = 'root';

        try {
            $db = new PDO($dsn, $usuario, $contraseña, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES  \'UTF8\''));
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            // die('Conectado');
        } catch (PDOException $e) {
            die ('Falló la conexión: ' . $e->getMessage());
        }
        return $db;
    }
}
