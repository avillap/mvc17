<?php

require_once 'models/User.php';
/**
*
*/
class LoginController
{

    function __construct()
    {
        # code...
    }


    public function index()
    {
        # formulario de login
        require 'views/login/index.php';
    }

    public function login()
    {
        //loguearse
        $user = User::validate($_POST['name'], $_POST['password']);

        if ($user) {
            $_SESSION['user'] = $user;
            header('Location: /home');

        } else {
            header('Location: /login');
        }
    }

    public function logout()
    {
        session_destroy();
        unset($_SESSION);
        header('Location: /login');
    }


}
