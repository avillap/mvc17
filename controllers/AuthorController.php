<?php

/**
*
*/
require_once('models/Author.php');

class AuthorController
{

    function __construct()
    {
        // echo "constructor AuthorController<br>";
    }

    public function index()
    {
        $authors = Author::all();
        require('views/author/index.php');
    }

    public function create()
    {
        require('views/author/create.php');
    }

    public function store()
    {
        // Crea un objeto de la clase Author.
        $author = new Author;
        // Vuelca en él el contenido del formulario.
        $author->name = $_POST['name'];
        $author->surname = $_POST['surname'];
        // $author->birthdate = $_POST['birthdate'];

        $author->birthdate = DateTime::createFromFormat('d-m-Y', $_POST['birthdate']);
        $author->store();
        header('location:index');
    }

    public function edit($id)
    {
        $author = Author::find($id);
        include 'views/author/edit.php';
    }

    public function update($id)
    {
        //buscar registro
        $author = Author::find($id);
        //actualizar campos
        $author->name = $_POST['name'];
        $author->surname = $_POST['surname'];
        $author->birthdate = $_POST['birthdate'];
        //ejecutar UPDATE
        $author->save();
        //redireccionar
        header('location:../index');
    }

    public function delete($id)
    {
        //buscar registro
        $author = Author::find($id);
        //ejecutar DELETE
        $author->delete();
        //redireccionar
        header('location:../index');
    }
}
