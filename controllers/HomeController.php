<?php

/**
*
*/
class HomeController
{

    function __construct()
    {

    }

    public function index()
    {
        if ($_SESSION['user']) {
            require "views/home.php";
        } else {
            header('Location: /login');
        }

    }
}
